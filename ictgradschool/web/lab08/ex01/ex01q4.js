"use strict";

// In ex01q4.js, write a program that prints all the factorials from 1! to 10! (where n! = n * (n-
//     1) * (n-2) * … * 2 * 1).

function factorialize(num) {
    var result = num;
    while (num > 1) { 
      num--;
      result *= num;
    }
    return result;
  }
  console.log(factorialize(1));
  console.log(factorialize(2));
  console.log(factorialize(3));
  console.log(factorialize(4));
  console.log(factorialize(5));
  console.log(factorialize(6));
  console.log(factorialize(7));
  console.log(factorialize(8));
  console.log(factorialize(9));
  console.log(factorialize(10));
