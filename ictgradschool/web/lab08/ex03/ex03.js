"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)
    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
      }
      
      console.log("your random number is " + getRandomInt(100));
      // expected output: 0 - 99
      

// TODO Complete this function, which should round the given number to 2dp and return the result.
function myFunction() {
    var num = 5.56789;
    var n = num.toFixed(2);
    
    console.log("rounded to 2dp " + n)
}


// TODO Write a function which calculates and returns the volume of a cone.

function cone(input) {
    let [r,h]=input.map(Number);
    let volume = Math.PI * r * r * h / 3;
    console.log("cone's volume = " + volume);
}
 
cone(["3", "7"]);

// TODO Write a function which calculates and returns the volume of a cylinder.

function cylinder(input) {
    let [r,h]=input.map(Number);
    let volume = Math.PI * r * r * h;
    console.log("cylinder's volume = " + volume);
}
 
cylinder(["1", "1"]);

// TODO Write a function which prints the name and volume of a shape, to 2dp.

// function print(input)


// console.log("the volume of the (shape) is (volume)")


// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.
